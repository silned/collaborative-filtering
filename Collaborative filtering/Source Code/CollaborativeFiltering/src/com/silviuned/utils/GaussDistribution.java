package com.silviuned.utils;

public class GaussDistribution {
	//public static final double EPSILON = Math.pow(0.1, 50);
	
	/*public static double pdf(double x, double mean, double variance) {
		if (variance > EPSILON) {
			double prefix = 1 / Math.sqrt(2 * Math.PI * variance);
			double power = (-1) * Math.pow(x - mean, 2) / (2 * variance);
			
			double result = prefix * Math.pow(Math.E, power);
			if (result > 1) {
				int asdf = 5;
			}
			return result;
		} else if (x < mean) {
			return 0;
		} else {
			return 1;
		}
	}*/

	public static double logPdf(double x, double mean, double variance) {
		if (variance != 0) {
			double result = -0.5 * Math.log(2 * Math.PI) - 0.5 * Math.log(variance)
				- Math.pow(x - mean, 2) / (2 * variance);
			return result;
		} else {
			return 0;
		}
	}

}
