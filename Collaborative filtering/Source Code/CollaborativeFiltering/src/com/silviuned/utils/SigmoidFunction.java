package com.silviuned.utils;

/**
 * Created by Silviu on 6/1/2017.
 */
public class SigmoidFunction {

    public static double eval(double x) {
        return 1.0 / (1 + Math.pow(Math.E, -x)) - 0.5;
    }

    public static double evalDerivative(double x) {
        double temp = eval(x);
        return temp * (1 - temp);
    }

    public static double inverse(double x) {
        return Math.log(x / (1 - x));
    }
}
