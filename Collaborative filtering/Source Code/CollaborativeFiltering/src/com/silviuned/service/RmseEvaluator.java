package com.silviuned.service;

import com.silviuned.model.UserRating;
import com.silviuned.service.algorithms.Algorithm;
import com.silviuned.service.postProcessing.GlobalBiasCorrector;
import com.silviuned.service.postProcessing.ItemBasedCorrector;
import com.silviuned.service.postProcessing.NearIntegerRounder;
import com.silviuned.service.postProcessing.PredictionTruncator;

import java.util.List;
import java.util.Map;

/**
 * Created by Silviu on 5/27/2017.
 */
public class RmseEvaluator {

    public static double eval(Algorithm algorithm, Map<Integer, List<UserRating>> data) {
        return eval(algorithm, data, null, null, false);
    }

    public static double eval(Algorithm algorithm, Map<Integer, List<UserRating>> data, ItemBasedCorrector itemBasedCorrector, GlobalBiasCorrector globalBiasCorrector, boolean useNearIntergerRounding) {
        int count = 0;
        double sum = 0;

        for (Map.Entry<Integer, List<UserRating>> entry : data.entrySet()) {
            int movieId = entry.getKey();
            for (UserRating userRating : entry.getValue()) {
                count++;
                Double prediction = algorithm.predictRating(movieId, userRating.getUserId());

                if (itemBasedCorrector != null){
                    prediction = itemBasedCorrector.correct(movieId, prediction);
                }

                if (globalBiasCorrector != null) {
                    prediction = globalBiasCorrector.correct(prediction);
                }

                prediction = PredictionTruncator.truncate(prediction);

                if (useNearIntergerRounding) {
                    prediction = NearIntegerRounder.round(prediction, algorithm.getNearIntegerRounderDiff());
                }

                sum += Math.pow(userRating.getRating() - prediction, 2);
            }
        }

        return sum / count;
    }

    public static double eval(double ratio, Map<Integer, List<UserRating>> data, Algorithm algorithm1, Algorithm algorithm2, ItemBasedCorrector itemBasedCorrector1, ItemBasedCorrector itemBasedCorrector2, GlobalBiasCorrector globalBiasCorrector1, GlobalBiasCorrector globalBiasCorrector2) {
        int count = 0;
        double sum = 0;

        for (Map.Entry<Integer, List<UserRating>> entry : data.entrySet()) {
            int movieId = entry.getKey();
            for (UserRating userRating : entry.getValue()) {
                count++;
                Double prediction1 = algorithm1.predictRating(movieId, userRating.getUserId());
                Double prediction2 = algorithm1.predictRating(movieId, userRating.getUserId());

                if (itemBasedCorrector1 != null){
                    prediction1 = itemBasedCorrector1.correct(movieId, prediction1);
                }
                if (globalBiasCorrector1 != null) {
                    prediction1 = globalBiasCorrector1.correct(prediction1);
                }
                prediction1 = PredictionTruncator.truncate(prediction1);
                prediction1 = NearIntegerRounder.round(prediction1, algorithm1.getNearIntegerRounderDiff());

                if (itemBasedCorrector2 != null){
                    prediction2 = itemBasedCorrector2.correct(movieId, prediction2);
                }
                if (globalBiasCorrector2 != null) {
                    prediction2 = globalBiasCorrector2.correct(prediction2);
                }
                prediction2 = PredictionTruncator.truncate(prediction2);
                prediction2 = NearIntegerRounder.round(prediction2, algorithm2.getNearIntegerRounderDiff());

                double prediction = ratio * prediction1 + (1 - ratio) * prediction2;

                sum += Math.pow(userRating.getRating() - prediction, 2);
            }
        }

        return sum / count;
    }
}
